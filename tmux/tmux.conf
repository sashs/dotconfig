#---------
# KEYBINDS
#---------
set -g mouse on
bind -n WheelUpPane if-shell -F -t = "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'select-pane -t=; copy-mode -e; send-keys -M'"
bind -n WheelDownPane select-pane -t= \; send-keys -M
bind -n C-WheelUpPane select-pane -t= \; copy-mode -e \; send-keys -M
bind -T copy-mode-vi    C-WheelUpPane   send-keys -X halfpage-up
bind -T copy-mode-vi    C-WheelDownPane send-keys -X halfpage-down
bind -T copy-mode-emacs C-WheelUpPane   send-keys -X halfpage-up
bind -T copy-mode-emacs C-WheelDownPane send-keys -X halfpage-down

# To copy, left click and drag to highlight text in yellow, 
# once you release left click yellow text will disappear and will automatically be available in clibboard
# # Use vim keybindings in copy mode
setw -g mode-keys vi
# Update default binding of `Enter` to also use copy-pipe
unbind -T copy-mode-vi Enter
bind-key -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "xclip -selection c"
bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "xclip -in -selection clipboard"

set -g status-keys vi
#setw -g mode-keys vi
#bind -n WheelUpPane if-shell -F -t = "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'copy-mode -e; send-keys -M'"
# Pipe the current pane to a log file with Shift-H - Press Shift-H again to stop.
bind-key H pipe-pane -o "cat >>$HOME/.tmux/#W-tmux.log" \; display-message "Toggled logging to $HOME/.tmux/#W-tmux.log"

bind F12 source-file ~/.tmux.conf

bind -n ^F1 select-window -t 1
bind -n ^F2 select-window -t 2
bind -n ^F3 select-window -t 3
bind -n ^F4 select-window -t 4
bind -n ^F5 select-window -t 5
bind -n ^F6 select-window -t 6
bind -n ^F7 select-window -t 7
bind -n ^F8 select-window -t 8
bind -n ^F9 select-window -t 9
bind -n ^F10 select-window -t 10
bind -n ^F11 select-window -t 11
bind -n ^F12 select-window -t 12

bind ^h select-pane -L
bind ^j select-pane -D
bind ^k select-pane -U
bind ^l select-pane -R

bind h resize-pane -L
bind j resize-pane -D
bind k resize-pane -U
bind l resize-pane -R

bind F1 select-pane -t 1
bind F2 select-pane -t 2
bind F3 select-pane -t 3
bind F4 select-pane -t 4
bind F5 select-pane -t 5
bind F6 select-pane -t 6
bind F7 select-pane -t 7
bind F8 select-pane -t 8
bind F9 select-pane -t 9
bind F10 select-pane -t 10
bind F11 select-pane -t 11

bind i split-window -h
bind s split-window

#----------------
# WINDOWS & PANES
#----------------

set -g pane-border-style fg=black
set -g pane-active-border-style fg=red
set -g display-panes-colour white
set -g display-panes-active-colour red
set -g display-panes-time 1000

setw -g automatic-rename on
setw -g monitor-activity on

#--------------
# MISC SETTINGS
#--------------

set -g set-titles on
set -g set-titles-string 'tmux: #T'
set -g repeat-time 100
set -g default-terminal "screen-256color"
setw -g clock-mode-colour red
setw -g clock-mode-style 12
setw -g alternate-screen on

# Don't close windows
#set -g set-remain-on-exit on

#-----------------
# STATUS & MESSAGE
#-----------------

set -g message-style fg=black,bg=yellow,bold

set -g status-justify right
set -g status-bg black
set -g status-fg white
set -g status-interval 5

setw -g window-status-current-style fg=red,none
setw -g window-status-format '#[fg=white]#I-#W#F'
setw -g window-status-current-format '#[fg=red] #I-#W#F '

set -g status-left '#[fg=white] #S#[fg=white]:#P #[fg=black,bold] > '
set -g status-left-length 40
set -g status-left-style fg=black
